<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['name', 'description'];

    public function vehicle()
    {
        return $this->hasMany('App\Vehicle');
    }
}
