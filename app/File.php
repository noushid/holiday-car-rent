<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name', 'type', 'date', 'size', 'gallery_id', 'vehicle_id'];

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
}
