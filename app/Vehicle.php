<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'name',
        'transmission',
        'displacement',
        'fuel',
        'bhp',
        'mileage',
        'torque',
        'engine',
        'price_per_day',
        'price_per_week',
        'seat',
        'center_lock',
        'audio_system',
        'video',
        'gps',
        'air_bag',
        'no_airbag',
        'extra',
        'brand_id'
    ];

    function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }

    public function newest_car()
    {
        return $this->hasOne('App\NewestCar');
    }

    public function file()
    {
        return $this->hasOne('App\File');
    }
}
