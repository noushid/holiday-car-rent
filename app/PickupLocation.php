<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickupLocation extends Model
{
    protected $fillable = ['name', 'description'];

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
}
