<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopularCar extends Model
{
    protected $fillable = ['vehicle_id'];

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
}
