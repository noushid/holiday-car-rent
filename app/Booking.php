<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillabel = ['name', 'email', 'phone', 'pickup_date', 'return_date', 'description', 'pickup_location_id', 'vehicle_id'];

    public function pickup_location()
    {
        return $this->belongsTo('App\PickupLocation');
    }

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

}
