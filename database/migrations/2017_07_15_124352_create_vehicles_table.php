<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('transmission');
            $table->string('displacement');
            $table->string('fuel');
            $table->string('bhp');
            $table->string('mileage');
            $table->string('torque');
            $table->string('engine');
            $table->decimal('price_per_day');
            $table->decimal('price_per_week');
            $table->string('seat');
            $table->boolean('center_lock');
            $table->boolean('audio_system');
            $table->boolean('video');
            $table->boolean('gps');
            $table->boolean('airbag');
            $table->string('no_airbag');
            $table->string('extra');
            $table->integer('brand_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
